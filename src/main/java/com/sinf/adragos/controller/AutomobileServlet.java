package com.sinf.adragos.controller;

import java.io.IOException;
import java.util.List;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.sinf.adragos.dao.AutomobilDao;
import com.sinf.adragos.model.Automobil;

/**
 * Servlet implementation class AutomobileServlet
 */
@WebServlet("/automobil")
public class AutomobileServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;
	private static final String AUTO_JSP = "masini.jsp";
	private AutomobilDao automobilDao;
	private List<Automobil> listaAutomobile;
       
    /**
     * @see HttpServlet#HttpServlet()
     */
    public AutomobileServlet() {
        super();
        automobilDao = new AutomobilDao();
        listaAutomobile = automobilDao.getAllAutomobil();
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		String action = String.valueOf(request.getAttribute("action"));
		switch (action) {
		case "delete":
			stergeAutomobil(request, response);
			break;
		default:
			listAutomobile(request, response);
			break;
		}
	}
	
	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		String add = request.getParameter("adaugam");
		String edit = request.getParameter("updatem");
		if (add != null) {
			automobilDao.addAutomobil(newAuto(request));
		} else if (edit != null) {
			automobilDao.updateAutomobil(newAuto(request));
		}
		doGet(request, response);
	}
	
	private Automobil newAuto(HttpServletRequest request) {
		Automobil automobil = new Automobil();
		if (request.getParameter("IdAutomobil")!= null) {
			automobil.setIdAutomobil(Integer.parseInt(request.getParameter("IdAutomobil")));
		}
		automobil.setMarca(request.getParameter("Marca"));
		automobil.setModel(request.getParameter("Model"));
		automobil.setPutere(request.getParameter("Putere"));
		automobil.setCarburant(request.getParameter("Carburant"));
		automobil.setPret(Double.parseDouble(request.getParameter("Pret")));
		return automobil;
	}

	private void stergeAutomobil(HttpServletRequest request, HttpServletResponse response) 
			throws ServletException, IOException {
		String idAutomobil = String.valueOf(request.getAttribute("idm"));
		automobilDao.deleteMasini(idAutomobil);
		listAutomobile(request, response);
	}

	private void listAutomobile(HttpServletRequest request, HttpServletResponse response) 
			throws ServletException, IOException {
		request.setAttribute("autom", listaAutomobile);
		request.getRequestDispatcher(AUTO_JSP).forward(request, response);
	}


	
}

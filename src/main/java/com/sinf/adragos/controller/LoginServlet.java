package com.sinf.adragos.controller;

import java.io.IOException;
import java.io.PrintWriter;
import java.util.List;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import com.sinf.adragos.dao.UtilizatoriDao;
import com.sinf.adragos.model.Utilizatori;

/**
 * Servlet implementation class LoginServlet
 */
@WebServlet("/login")
public class LoginServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;
	private static final String LOGIN_JSP = "/login.jsp";
	private static final String INDEX_JSP = "/index.jsp";
	private UtilizatoriDao utilizatoriDao;
	private List<Utilizatori> listaUtilizatori;
	
	

	public LoginServlet() {
		super();
		utilizatoriDao = new UtilizatoriDao();
		listaUtilizatori = utilizatoriDao.getAllUsers();
	}

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		response.setContentType("text/html");
		PrintWriter out = response.getWriter();

		HttpSession session = request.getSession();
		session.invalidate();

		request.getRequestDispatcher(LOGIN_JSP).forward(request, response);
		out.print("Ai fost deconectat cu succes!");
		out.close();
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		String email = request.getParameter("email");
		int pass = Integer.parseInt(request.getParameter("password"));

		for (Utilizatori u : listaUtilizatori) {
			if (u.getEmail().equals(email)) {
				if (u.getParola().equals(pass)) {
					HttpSession session = request.getSession();
					session.setAttribute("user", u);
					request.getRequestDispatcher(INDEX_JSP).forward(request, response);
					return;
				} else {
					request.setAttribute("err", "User sau parola gresite");
					request.getRequestDispatcher(LOGIN_JSP).forward(request, response);
					return;
				}
			}
		}
	}

}

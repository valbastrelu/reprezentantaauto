package com.sinf.adragos.dao;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;

import com.sinf.adragos.model.Angajat;
import com.sinf.adragos.util.DbUtil;

public class AngajatDao {
	private Connection connection;

	public AngajatDao(Connection connection) {
		this.connection = DbUtil.connect();
	}
	
	/**
	 * JAVADOC
	 * 
	 * @param automobil 
	 */
	public void addAngajat(Angajat angajat) {
		try {
			PreparedStatement preparedStatement = connection
					.prepareStatement("INSERT INTO angajat (Nume,Prenume,Adresa,Vechime) values (?, ?, ?, ?)");
			// Parameters start with 1
			preparedStatement.setString(1, angajat.getNume());
			preparedStatement.setString(2, angajat.getPrenume());
			preparedStatement.setString(3, angajat.getAdresa());
			preparedStatement.setInt(4, angajat.getVechime());
			preparedStatement.executeUpdate();

		} catch (SQLException e) {
			e.printStackTrace();
		}
	}

	/**
	 * Creeaza si executa o comanda UPDATE in mauto.masina pentru a modifica un
	 * obiect de tip Masina
	 * 
	 * @param masina Obiect folosit la editare.
	 */
	public void updateAngajat(Angajat angajat) {
		try {
			PreparedStatement preparedStatement = connection
					.prepareStatement("UPDATE angajat SET Nume=?,Prenume=?,Adresa=?,Vechime=? where IdAngajat=?");
			// Parameters start with 1
			preparedStatement.setString(1, angajat.getNume());
			preparedStatement.setString(2, angajat.getPrenume());
			preparedStatement.setString(3, angajat.getAdresa());
			preparedStatement.setInt(4, angajat.getVechime());
			preparedStatement.executeUpdate();

		} catch (SQLException e) {
			e.printStackTrace();
		}
	}

	/**
	 * Construieste o lista de masini aflate in baza de date mauto.masina
	 * 
	 * @return Intoarce o lista cu toate masinile.
	 */
	public List<Angajat> getAllAngajat() {
		List<Angajat> angajati = new ArrayList<Angajat>();
		try {
			Statement statement = connection.createStatement();
			ResultSet rs = statement.executeQuery("SELECT * FROM angajat");
			while (rs.next()) {
				Angajat angajat = new Angajat();
				angajat.setIdAngajat(rs.getInt("IdAngajat"));
				angajat.setNume(rs.getString("Nume"));
				angajat.setPrenume(rs.getString("Prenume"));
				angajat.setAdresa(rs.getString("Adresa"));
				angajat.setVechime(rs.getInt("Vechime"));
				angajati.add(angajat);
			}
		} catch (SQLException e) {
			e.printStackTrace();
		}
		return angajati;
	}

	/**
	 * Creeaza si executa o comanda DELETE in mauto.masina pentru a sterge un
	 * obiect de tip Masina
	 * 
	 * @param codm Un String sub forma unei liste de coduri. Folosit la stergere.
	 */
	public void deleteAngajat(int idAngajat) {
		try {
			String query = String.format("DELETE FROM angajat WHERE IdAngajat = ", idAngajat);
			PreparedStatement ps = connection.prepareStatement(query);
			ps.executeUpdate();

		} catch (SQLException e) {
			e.printStackTrace();
		}
	}
}

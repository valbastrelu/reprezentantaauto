package com.sinf.adragos.dao;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;

import com.sinf.adragos.model.Piesa;
import com.sinf.adragos.util.DbUtil;

public class PiesaDao {
	private Connection connection;

	public PiesaDao(Connection connection) {
		this.connection = DbUtil.connect();
	}
	
	/**
	 * JAVADOC
	 * 
	 * @param automobil 
	 */
	public void addPiesa(Piesa piesa) {
		try {
			PreparedStatement preparedStatement = connection
					.prepareStatement("INSERT INTO piesa (Nume,Pret) values (?, ?)");
			// Parameters start with 1
			preparedStatement.setString(1, piesa.getNume());
			preparedStatement.setDouble(2, piesa.getPret());
			preparedStatement.executeUpdate();

		} catch (SQLException e) {
			e.printStackTrace();
		}
	}

	/**
	 * Creeaza si executa o comanda UPDATE in mauto.masina pentru a modifica un
	 * obiect de tip Masina
	 * 
	 * @param masina Obiect folosit la editare.
	 */
	public void updatePiesa(Piesa piesa) {
		try {
			PreparedStatement preparedStatement = connection
					.prepareStatement("UPDATE piesa SET Nume=?,Pret=? where IdPiesa=?");
			// Parameters start with 1
			preparedStatement.setString(1, piesa.getNume());
			preparedStatement.setDouble(2, piesa.getPret());
			preparedStatement.executeUpdate();

		} catch (SQLException e) {
			e.printStackTrace();
		}
	}

	/**
	 * Construieste o lista de masini aflate in baza de date mauto.masina
	 * 
	 * @return Intoarce o lista cu toate masinile.
	 */
	public List<Piesa> getAllPiesa() {
		List<Piesa> piese = new ArrayList<Piesa>();
		try {
			Statement statement = connection.createStatement();
			ResultSet rs = statement.executeQuery("SELECT * FROM piesa");
			while (rs.next()) {
				Piesa piesa = new Piesa();
				piesa.setIdPiesa(rs.getInt("IdPiesa"));
				piesa.setNume(rs.getString("Nume"));
				piesa.setPret(rs.getDouble("Pret"));
				piese.add(piesa);
			}
		} catch (SQLException e) {
			e.printStackTrace();
		}
		return piese;
	}

	/**
	 * Creeaza si executa o comanda DELETE in mauto.masina pentru a sterge un
	 * obiect de tip Masina
	 * 
	 * @param codm Un String sub forma unei liste de coduri. Folosit la stergere.
	 */
	public void deletePiesa(int IdPiesa) {
		try {
			String query = String.format("DELETE FROM piesa WHERE IdPiesa = ", IdPiesa);
			PreparedStatement ps = connection.prepareStatement(query);
			ps.executeUpdate();

		} catch (SQLException e) {
			e.printStackTrace();
		}
	}
}

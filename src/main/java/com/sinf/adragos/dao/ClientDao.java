package com.sinf.adragos.dao;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;

import com.sinf.adragos.model.Client;
import com.sinf.adragos.util.DbUtil;

public class ClientDao {
	private Connection connection;

	public ClientDao(Connection connection) {
		this.connection = DbUtil.connect();
	}
	
	/**
	 * JAVADOC
	 * 
	 * @param client 
	 */
	public void addClient(Client client) {
		try {
			PreparedStatement preparedStatement = connection
					.prepareStatement("INSERT INTO client (Nume,Prenume,Varsta,CNP,SEX) values (?, ?, ?, ?, ?)");
			// Parameters start with 1
			preparedStatement.setString(1, client.getNume());
			preparedStatement.setString(2, client.getPrenume());
			preparedStatement.setInt(3, client.getVarsta());
			preparedStatement.setString(4, client.getCNP());
			preparedStatement.setString(5, client.getSEX());
			preparedStatement.executeUpdate();

		} catch (SQLException e) {
			e.printStackTrace();
		}
	}

	/**
	 * Creeaza si executa o comanda UPDATE in mauto.masina pentru a modifica un
	 * obiect de tip Masina
	 * 
	 * @param masina Obiect folosit la editare.
	 */
	public void updateClient(Client client) {
		try {
			PreparedStatement preparedStatement = connection
					.prepareStatement("UPDATE client SET Nume=?,Prenume=?,Varsta=?,CNP=?,SEX=? where IdClient=?");
			// Parameters start with 1
			preparedStatement.setString(1, client.getNume());
			preparedStatement.setString(2, client.getPrenume());
			preparedStatement.setInt(3, client.getVarsta());
			preparedStatement.setString(4, client.getCNP());
			preparedStatement.setString(5, client.getSEX());
			preparedStatement.executeUpdate();

		} catch (SQLException e) {
			e.printStackTrace();
		}
	}

	/**
	 * Construieste o lista de masini aflate in baza de date mauto.masina
	 * 
	 * @return Intoarce o lista cu toate masinile.
	 */
	public List<Client> getAllClient() {
		List<Client> clienti = new ArrayList<Client>();
		try {
			Statement statement = connection.createStatement();
			ResultSet rs = statement.executeQuery("SELECT * FROM client");
			while (rs.next()) {
				Client client = new Client();
				client.setIdClient(rs.getInt("IdClient"));
				client.setNume(rs.getString("Nume"));
				client.setPrenume(rs.getString("Prenume"));
				client.setVarsta(rs.getInt("Varsta"));
				client.setCNP(rs.getString("CNP"));
				client.setSEX(rs.getString("SEX"));
				clienti.add(client);
			}
		} catch (SQLException e) {
			e.printStackTrace();
		}
		return clienti;
	}

	/**
	 * Creeaza si executa o comanda DELETE in mauto.masina pentru a sterge un
	 * obiect de tip Masina
	 * 
	 * @param codm Un String sub forma unei liste de coduri. Folosit la stergere.
	 */
	public void deleteClient(int idClient) {
		try {
			String query = String.format("DELETE FROM client WHERE IdClient = ", idClient);
			PreparedStatement ps = connection.prepareStatement(query);
			ps.executeUpdate();

		} catch (SQLException e) {
			e.printStackTrace();
		}
	}
}

package com.sinf.adragos.dao;

import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;

import com.sinf.adragos.model.Utilizatori;
import com.sinf.adragos.util.DbUtil;

public class UtilizatoriDao {
	private Connection connection;

	public UtilizatoriDao() {
		super();
		this.connection = DbUtil.connect();
	}
	
	/**
	 * Construieste un obiect de tip User din baza de date mauto.user
	 * @param email Folosit in interogarea SQL pentru a aduce user
	 * @return User Intoarce un obiect de tip User
	 */
	public Utilizatori getUserByEmail(String email) {
		Utilizatori user = new Utilizatori();

		try {
			Statement st = connection.createStatement();
			ResultSet rs = st.executeQuery("SELECT * FROM utilizatori WHERE email = '" + email + "'");
			if (rs.next()) {
				user.setIdu(rs.getInt("idu"));
				user.setNume(rs.getString("nume"));
				user.setPrenume(rs.getString("prenume"));
				user.setEmail(rs.getString("email"));
				user.setParola("parola");
				user.setActiv(rs.getInt("activ"));
			}

		} catch (Exception e) {
			e.printStackTrace();
		}
		return user;
	}

	/**
	 * Construieste o lista de masini aflate din baza de date mauto.masina
	 * 
	 * @return Intoarce o lista cu toate masinile.
	 */
	public List<Utilizatori> getAllUsers() {
		List<Utilizatori> users = new ArrayList<Utilizatori>();
		try {
			Statement statement = connection.createStatement();
			ResultSet rs = statement.executeQuery("SELECT * FROM utilizatori");
			while (rs.next()) {
				Utilizatori user = new Utilizatori();
				user.setIdu(rs.getInt("idu"));
				user.setNume(rs.getString("nume"));
				user.setPrenume(rs.getString("prenume"));
				user.setEmail(rs.getString("email"));
				user.setParola("parola");
				user.setActiv(rs.getInt("activ"));
				users.add(user);
			}
		} catch (SQLException e) {
			e.printStackTrace();
		}
		return users;
	}
}

package com.sinf.adragos.dao;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;

import com.sinf.adragos.model.Automobil;
import com.sinf.adragos.util.DbUtil;

public class AutomobilDao {
	private Connection connection;

	public AutomobilDao() {
		this.connection = DbUtil.connect();
	}
	
	/**
	 * JAVADOC
	 * 
	 * @param automobil 
	 */
	public void addAutomobil(Automobil automobil) {
		try {
			PreparedStatement preparedStatement = connection
					.prepareStatement("INSERT INTO automobil (Marca,Model,Putere,Carburant,Pret) values (?, ?, ?, ?, ?)");
			// Parameters start with 1
			preparedStatement.setString(1, automobil.getMarca());
			preparedStatement.setString(2, automobil.getModel());
			preparedStatement.setString(3, automobil.getPutere());
			preparedStatement.setString(4, automobil.getCarburant());
			preparedStatement.setDouble(5, automobil.getPret());
			preparedStatement.executeUpdate();

		} catch (SQLException e) {
			e.printStackTrace();
		}
	}

	/**
	 * Creeaza si executa o comanda UPDATE in mauto.masina pentru a modifica un
	 * obiect de tip Masina
	 * 
	 * @param masina Obiect folosit la editare.
	 */
	public void updateAutomobil(Automobil automobil) {
		try {
			PreparedStatement preparedStatement = connection
					.prepareStatement("UPDATE automobil SET Marca=?,Model=?,Putere=?,Carburant=?,Pret=? "
							+ "WHERE idAutomobil=?");
			// Parameters start with 1
			preparedStatement.setString(1, automobil.getMarca());
			preparedStatement.setString(2, automobil.getModel());
			preparedStatement.setString(3, automobil.getPutere());
			preparedStatement.setString(4, automobil.getCarburant());
			preparedStatement.setDouble(5, automobil.getPret());
			preparedStatement.setInt(6, automobil.getIdAutomobil());
			preparedStatement.executeUpdate();

		} catch (SQLException e) {
			e.printStackTrace();
		}
	}

	/**
	 * Construieste o lista de masini aflate in baza de date mauto.masina
	 * 
	 * @return Intoarce o lista cu toate masinile.
	 */
	public List<Automobil> getAllAutomobil() {
		List<Automobil> automobile = new ArrayList<Automobil>();
		try {
			Statement statement = connection.createStatement();
			ResultSet rs = statement.executeQuery("SELECT * FROM automobil");
			while (rs.next()) {
				Automobil automobil = new Automobil();
				automobil.setIdAutomobil(rs.getInt("IdAutomobil"));
				automobil.setMarca(rs.getString("Marca"));
				automobil.setModel(rs.getString("Model"));
				automobil.setPutere(rs.getString("Putere"));
				automobil.setCarburant(rs.getString("Carburant"));
				automobil.setPret(rs.getDouble("Pret"));
				automobile.add(automobil);
			}
		} catch (SQLException e) {
			e.printStackTrace();
		}
		return automobile;
	}

	/**
	 * Creeaza si executa o comanda DELETE in mauto.masina pentru a sterge un
	 * obiect de tip Masina
	 * 
	 * @param codm Un String sub forma unei liste de coduri. Folosit la stergere.
	 */
	public void deleteMasini(String idAutomobil) {
		try {
			String query = String.format("DELETE FROM automobil WHERE IdAutomobil = ", idAutomobil);
			PreparedStatement ps = connection.prepareStatement(query);
			ps.executeUpdate();

		} catch (SQLException e) {
			e.printStackTrace();
		}
	}
}

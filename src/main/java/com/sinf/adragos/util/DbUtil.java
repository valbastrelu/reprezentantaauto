package com.sinf.adragos.util;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;

/**
 * Clasa pentru realizarea conexiunii cu baza de date.
 * 
 * @author Crow
 *
 */
public class DbUtil {

	private static Connection connection = null;
	private final static String DATABASE_DRIVER = "com.mysql.jdbc.Driver";
	private final static String DATABASE_URL = "jdbc:mysql://localhost:3306/reprezentantaauto";
	private final static String DATABASE_USER = "root";
	private final static String DATABASE_PASS = "";
	
    /**
     * Verifica daca exista o conexiune si instantiaza o conexiune noua MySQL.
     * 
     * @return Intoarce un obiect de tip Connection.
     */
    public static Connection connect() {
        if (connection == null) {
            try {
                Class.forName(DATABASE_DRIVER);
                connection = DriverManager.getConnection(DATABASE_URL, DATABASE_USER, DATABASE_PASS);
            } catch (ClassNotFoundException e) {
                e.printStackTrace();
            } catch (SQLException e){
            	e.printStackTrace();
            }
        }
        return connection;
    }

    /**
     * Verifica daca exista o conexiune si incearca sa o inchida si sa goleasca obiectul.
     */
    public static void disconnect() {
        if (connection != null) {
            try {
                connection.close();
                connection = null;
            } catch (SQLException e) {
                e.printStackTrace();
            }
        }
    }
}

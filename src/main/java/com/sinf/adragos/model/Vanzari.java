package com.sinf.adragos.model;

import java.sql.Date;

public class Vanzari {
	private int IdVanzari;
	private int IdClient;
	private int IdAutomobil;
	private int IdAngajat;
	private Date Data;
	public int getIdVanzari() {
		return IdVanzari;
	}
	public void setIdVanzari(int idVanzari) {
		IdVanzari = idVanzari;
	}
	public int getIdClient() {
		return IdClient;
	}
	public void setIdClient(int idClient) {
		IdClient = idClient;
	}
	public int getIdAutomobil() {
		return IdAutomobil;
	}
	public void setIdAutomobil(int idAutomobil) {
		IdAutomobil = idAutomobil;
	}
	public int getIdAngajat() {
		return IdAngajat;
	}
	public void setIdAngajat(int idAngajat) {
		IdAngajat = idAngajat;
	}
	public Date getData() {
		return Data;
	}
	public void setData(Date data) {
		Data = data;
	}
	@Override
	public String toString() {
		return "Vanzari [IdVanzari=" + IdVanzari + ", IdClient=" + IdClient + ", IdAutomobil=" + IdAutomobil
				+ ", IdAngajat=" + IdAngajat + ", Data=" + Data + "]";
	}
}

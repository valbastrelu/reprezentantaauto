package com.sinf.adragos.model;

public class Functii {
	private int IdFunctie;
	private int IdAngajat;
	private String Denumire;
	
	public int getIdFunctie() {
		return IdFunctie;
	}
	public void setIdFunctie(int idFunctie) {
		IdFunctie = idFunctie;
	}
	public int getIdAngajat() {
		return IdAngajat;
	}
	public void setIdAngajat(int idAngajat) {
		IdAngajat = idAngajat;
	}
	public String getDenumire() {
		return Denumire;
	}
	public void setDenumire(String denumire) {
		Denumire = denumire;
	}
	@Override
	public String toString() {
		return "Functii [IdFunctie=" + IdFunctie + ", IdAngajat=" + IdAngajat + ", Denumire=" + Denumire + "]";
	}
}

package com.sinf.adragos.model;

public class Client {
	private int idClient;
	private String Nume;
	private String Prenume;
	private int Varsta;
	private String CNP;
	private String SEX;
	public int getIdClient() {
		return idClient;
	}
	public void setIdClient(int idClient) {
		this.idClient = idClient;
	}
	public String getNume() {
		return Nume;
	}
	public void setNume(String nume) {
		Nume = nume;
	}
	public String getPrenume() {
		return Prenume;
	}
	public void setPrenume(String prenume) {
		Prenume = prenume;
	}
	public int getVarsta() {
		return Varsta;
	}
	public void setVarsta(int varsta) {
		Varsta = varsta;
	}
	public String getCNP() {
		return CNP;
	}
	public void setCNP(String CNP) {
		this.CNP = CNP;
	}
	public String getSEX() {
		return SEX;
	}
	public void setSEX(String SEX) {
		this.SEX = SEX;
	}
	@Override
	public String toString() {
		return "Client [idClient=" + idClient + ", Nume=" + Nume + ", Prenume=" + Prenume + ", Varsta=" + Varsta
				+ ", CNP=" + CNP + ", SEX=" + SEX + "]";
	}
}

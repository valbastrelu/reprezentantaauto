package com.sinf.adragos.model;

public class Stoc {
	private int IdStoc;
	private int IdFurnizor;
	private int IdAutomobil;
	private int Cantitate;
	
	public int getIdStoc() {
		return IdStoc;
	}
	public void setIdStoc(int idStoc) {
		IdStoc = idStoc;
	}
	public int getIdFurnizor() {
		return IdFurnizor;
	}
	public void setIdFurnizor(int idFurnizor) {
		IdFurnizor = idFurnizor;
	}
	public int getIdAutomobil() {
		return IdAutomobil;
	}
	public void setIdAutomobil(int idAutomobil) {
		IdAutomobil = idAutomobil;
	}
	public int getCantitate() {
		return Cantitate;
	}
	public void setCantitate(int cantitate) {
		Cantitate = cantitate;
	}
	@Override
	public String toString() {
		return "Stoc [IdStoc=" + IdStoc + ", IdFurnizor=" + IdFurnizor + ", IdAutomobil=" + IdAutomobil + ", Cantitate="
				+ Cantitate + "]";
	}
}

package com.sinf.adragos.model;

public class Angajat {
	private int idAngajat;
	private String Nume;
	private String Prenume;
	private String Adresa;
	private int Vechime;
	
	public int getIdAngajat() {
		return idAngajat;
	}
	public void setIdAngajat(int idAngajat) {
		this.idAngajat = idAngajat;
	}
	public String getNume() {
		return Nume;
	}
	public void setNume(String nume) {
		Nume = nume;
	}
	public String getPrenume() {
		return Prenume;
	}
	public void setPrenume(String prenume) {
		Prenume = prenume;
	}
	public String getAdresa() {
		return Adresa;
	}
	public void setAdresa(String adresa) {
		Adresa = adresa;
	}
	public int getVechime() {
		return Vechime;
	}
	public void setVechime(int vechime) {
		Vechime = vechime;
	}
	@Override
	public String toString() {
		return "Angajat [idAngajat=" + idAngajat + ", Nume=" + Nume + ", Prenume=" + Prenume + ", Adresa=" + Adresa
				+ ", Vechime=" + Vechime + "]";
	}

}

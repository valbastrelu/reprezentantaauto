package com.sinf.adragos.model;

public class Automobil {
	private int idAutomobil;
	private String Marca;
	private String Model;
	private String Putere;
	private String Carburant;
	private double Pret;
	public int getIdAutomobil() {
		return idAutomobil;
	}
	public void setIdAutomobil(int idAutomobil) {
		this.idAutomobil = idAutomobil;
	}
	public String getMarca() {
		return Marca;
	}
	public void setMarca(String marca) {
		Marca = marca;
	}
	public String getModel() {
		return Model;
	}
	public void setModel(String model) {
		Model = model;
	}
	public String getPutere() {
		return Putere;
	}
	public void setPutere(String putere) {
		Putere = putere;
	}
	public String getCarburant() {
		return Carburant;
	}
	public void setCarburant(String carburant) {
		Carburant = carburant;
	}
	public double getPret() {
		return Pret;
	}
	public void setPret(double pret) {
		Pret = pret;
	}
	@Override
	public String toString() {
		return "Automobil [idAutomobil=" + idAutomobil + ", Marca=" + Marca + ", Model=" + Model + ", Putere=" + Putere
				+ ", Carburant=" + Carburant + ", Pret=" + Pret + "]";
	}
}

package com.sinf.adragos.model;

public class Piesa {
	private int IdPiesa;
	private String Nume;
	private double Pret;
	
	public int getIdPiesa() {
		return IdPiesa;
	}
	public void setIdPiesa(int idPiesa) {
		IdPiesa = idPiesa;
	}
	public String getNume() {
		return Nume;
	}
	public void setNume(String nume) {
		Nume = nume;
	}
	public double getPret() {
		return Pret;
	}
	public void setPret(double pret) {
		Pret = pret;
	}
	@Override
	public String toString() {
		return "Piesa [IdPiesa=" + IdPiesa + ", Nume=" + Nume + ", Pret=" + Pret + "]";
	}
}

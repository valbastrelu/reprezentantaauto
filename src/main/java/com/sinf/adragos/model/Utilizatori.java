package com.sinf.adragos.model;

public class Utilizatori {
	private int idu;
	private String nume;
	private String prenume;
	private String email;
	private String parola;
	private int activ;
	public int getIdu() {
		return idu;
	}
	public void setIdu(int idu) {
		this.idu = idu;
	}
	public String getNume() {
		return nume;
	}
	public void setNume(String nume) {
		this.nume = nume;
	}
	public String getPrenume() {
		return prenume;
	}
	public void setPrenume(String prenume) {
		this.prenume = prenume;
	}
	public String getEmail() {
		return email;
	}
	public void setEmail(String email) {
		this.email = email;
	}
	public String getParola() {
		return parola;
	}
	public void setParola(String parola) {
		this.parola = parola;
	}
	public int getActiv() {
		return activ;
	}
	public void setActiv(int activ) {
		this.activ = activ;
	}
	@Override
	public String toString() {
		return "Utilizatori [idu=" + idu + ", nume=" + nume + ", prenume=" + prenume + ", email=" + email + ", parola="
				+ parola + ", activ=" + activ + "]";
	}
}

package com.sinf.adragos.model;

public class Furnizor {
	private int IdFurnizor;
	private String Denumire;
	private String Adresa;
	
	public int getIdFurnizor() {
		return IdFurnizor;
	}
	public void setIdFurnizor(int idFurnizor) {
		IdFurnizor = idFurnizor;
	}
	public String getDenumire() {
		return Denumire;
	}
	public void setDenumire(String denumire) {
		Denumire = denumire;
	}
	public String getAdresa() {
		return Adresa;
	}
	public void setAdresa(String adresa) {
		Adresa = adresa;
	}
	@Override
	public String toString() {
		return "Furnizor [IdFurnizor=" + IdFurnizor + ", Denumire=" + Denumire + ", Adresa=" + Adresa + "]";
	}
}

<%@page import="java.util.List"%>
<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ page session="true"%>
<%@ page isELIgnored="false"%>
<html>
<head>
<link rel="stylesheet" type="text/css" href="resources/css/login.css">
<script src="resources/js/login.js"></script>
<title>Reprezentata AUTO</title>
</head>
<body>
	<div class="login-page">
		<div class="form">
			<form class="login-form" method="POST" action="login">
				<input type="text" name="email" placeholder="email" /> 
				<input type="password" name="password" placeholder="password" />
				<button name="login">login</button>
			</form>
		</div>
	</div>
</body>
</html>
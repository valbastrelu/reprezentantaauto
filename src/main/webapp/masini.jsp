<%@page import="java.util.List"%>
<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ page session="true"%>
<%@ page isELIgnored="false"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
	<link rel="stylesheet" type="text/css" href="resources/css/navbar.css">
	<link rel="stylesheet" type="text/css" href="resources/css/tabel.css">
	<link rel="stylesheet" type="text/css" href="resources/css/butoane.css">
	<script src="js/tabel.js"></script>
	<script src="js/functii.js"></script>
</head>
<body>
	<jsp:include page="navbar.jsp"></jsp:include>
	<div class='main'>
    <section class="content">
      <h1>Masini</h1>
    </section>
    <table class="heavyTable" id='myTable'>
			<input type='text' id='myInput' onkeyup='myFunction()' placeholder='Cauta dupa marca...'></input>
			<thead><tr>
			<th>ID Automobil</th>
			<th>Marca</th>
			<th>Model</th>
			<th>Putere</th>
			<th>Carburant</th>
			<th>Pret</th>
			<th>CRUD</th></tr>
			</thead>
			<tbody>
				<form action='automobil' method='POST'><tr id='edit' style="display:none;">
					<td><input class='edit' type='text' name='IdAutomobil' value='' readonly></td>
					<td><input class='edit' type='text' name='Marca' value=''></td>
					<td><input class='edit' type='text' name='Model' value=''></td>
					<td><input class='edit' type='text' name='Putere' value=''></td>
					<td><input class='edit' type='text' name='Carburant' value=''></td>
					<td><input class='edit' type='text' name='Pret' value=''></td>
					<td><div class='btn'>
					<ul><li><input type ='submit' name='updatem' value='Update'></li>
					<li><a href='masini.jsp'>Cancel</a></li></ul></div></td>
				</tr></form>
				<c:forEach var="auto" items="${autom}">
					<form action='automobil' method='POST'><tr>
						<td><c:out value="${auto.idAutomobil}" /></td>
						<td style="text-transform: capitalize"><c:out value="${auto.marca}" /></td>
						<td style="text-transform: capitalize"><c:out value="${auto.model}" /></td>
						<td><c:out value="${auto.putere}" /></td>
						<td><c:out value="${auto.carburant}" /></td>
						<td><c:out value="${auto.pret}" /></td>
						<td><div class='btn'><ul>
							<li><a href="/automobil?action=delete&idm=" + ${auto.idAutomobil}>Sterge</a></li>
							<li><a onClick="editeaza(${auto.idAutomobil})">Modifica</a></li></ul></div>
						</td>
					</tr>
				</c:forEach>
				</form>
			<form action='automobil' method='POST'><tr>
				<td>NOU: </td>
				<td><input type='text' name='Marca' placeholder='Marca'></td>
				<td><input type='text' name='Model' placeholder='Model'></td>
				<td><input type='text' name='Putere' placeholder='Putere'></td>
				<td><input type='text' name='Carburant' placeholder='Carburant'></td>
				<td><input type='text' name='Pret' placeholder='Pret'></td>
				<td><div class='btn'><ul>
					<li><input class='buton' type ='submit' id='add' name = 'adaugam' value='Adauga'></li>
				</ul></div></td>
				</tr></form>
			</tbody>
    </table>
  </div>
</body>
</html>
</html>